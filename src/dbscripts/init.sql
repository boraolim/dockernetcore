CREATE TABLE Blog
(
 id serial PRIMARY KEY,
 title VARCHAR (50) NOT NULL,
 description VARCHAR (100) NOT NULL
);

ALTER TABLE Blog OWNER TO admin;
Insert into Blog (title, description) values('Title 1', 'Description 1');
Insert into Blog (title, description) values('Title 2', 'Description 2');
Insert into Blog (title, description) values('Title 3', 'Description 3');
Insert into Blog (title, description) values('Title 4', 'Description 4');
Insert into Blog (title, description) values('Title 5', 'Description 5');
Insert into Blog (title, description) values('Title 6', 'Description 6');

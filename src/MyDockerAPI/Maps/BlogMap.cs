namespace MyDockerAPI.Maps
{
  using MyDockerAPI.Models;
  using Microsoft.EntityFrameworkCore;
  using Microsoft.EntityFrameworkCore.Metadata.Builders;
  public class BlogMap
  {
    public BlogMap(EntityTypeBuilder<Blog> entityBuilder)
    {
      entityBuilder.ToTable("blog");
      entityBuilder.Property(x => x.Id).HasColumnName("id");
      entityBuilder.Property(x => x.Title).HasColumnName("title");
      entityBuilder.Property(x => x.Description).HasColumnName("description");
      entityBuilder.HasKey(x => x.Id);
    }
  }
}
namespace MyDockerAPI.Controllers
{
  using MyDockerAPI.Models;
  using Microsoft.AspNetCore.Mvc;
  using System.Linq;

  [ApiController]
  [Route("api/[controller]")]
  public class BlogController : ControllerBase
  {
    private readonly ApiDbContext _context;
    public BlogController(ApiDbContext context) => _context = context;

    [HttpGet]
    public object GetBlogs()
    {
      return _context.Blogs.Where(b => b.Title.Contains("Title")).Select((c) => new
      {
        Id = c.Id,
        Title = c.Title,
        Description = c.Description
      }).ToList();
    }
  }
}